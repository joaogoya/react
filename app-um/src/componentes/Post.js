import React from 'react';
import Comment from './Comment';

export default class Post extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            comments:[
                { text: 'Bom post' },
                { text: 'Tb acho' }
            ],
            newCommentText: '',
        }
        this.handlesubmit = this.handlesubmit.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
    }

    handlesubmit(e){
        
        this.setState({
            comments: [
                ... this.state.comments,
                { text: this.state.newCommentText }
            ]
        });

        this.setState(
            { newCommentText: '' }
        );

        e.preventDefault();

    }

    handleTextChange(e){
        this.setState(
            { newCommentText: e.target.value }
        );
    }

    render (){
        return (
            <div>
                <h1> { this.props.title } </h1>
                <form onSubmit={ this.handlesubmit }>
                    <input type="text" value={ this.newCommentText } onChange={ this.handleTextChange } />
                    <br/>
                    <button type="submit">Envia comentario</button>
                </form>

                { this.state.comments.map((comment, index) => {
                    return <Comment key={ index } text={ comment.text } />
                })}
            </div>
        )
    }
} 